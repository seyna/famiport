class CreatePuzzles < ActiveRecord::Migration
  def change
    create_table :puzzles do |t|
    	t.integer :activity_id
    	t.string  :name
    	t.integer :total_pieces

      t.timestamps null: false
    end

    add_index :puzzles, :activity_id
  end
end
