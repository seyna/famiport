class CreatePrizes < ActiveRecord::Migration
  def change
    create_table :prizes do |t|
    	t.text :name
    	t.text :description
    	t.integer :puzzle_id
    	t.attachment :image
      t.timestamps null: false
    end

    add_index :prizes, :puzzle_id
  end
end
