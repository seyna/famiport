class AddPhoneNumberToUser < ActiveRecord::Migration
  def change
  	add_column :users, :serial_number, :string
  	add_column :users, :phone_number, :string  	
  end
end
