class CreatePuzzleimages < ActiveRecord::Migration
  def change
    create_table :puzzleimages do |t|
    	t.integer :puzzle_id
    	t.integer :position_number
    	t.attachment :image
      t.timestamps null: false
    end

    add_index :puzzleimages, :puzzle_id
  end
end
