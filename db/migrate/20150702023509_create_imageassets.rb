class CreateImageassets < ActiveRecord::Migration
  def change
    create_table :imageassets do |t|
    	t.string :description
    	t.attachment :asset

      t.timestamps null: false
    end
  end
end
