class CreateLotterywinners < ActiveRecord::Migration
  def change
    create_table :lotterywinners do |t|
    	t.integer :activity_id
    	t.integer :user_id
    	t.integer :prize_id

      t.timestamps null: false
    end

    add_index :lotterywinners, :activity_id
    add_index :lotterywinners, :user_id
    add_index :lotterywinners, :prize_id
  end
end
