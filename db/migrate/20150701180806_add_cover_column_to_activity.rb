class AddCoverColumnToActivity < ActiveRecord::Migration
  def up
		add_attachment :activities, :cover
  end

  def down
  	remove_attachment :activities, :cover
  end
end
