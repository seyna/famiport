class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
    	t.string :name
    	t.datetime :start_time
    	t.datetime :end_time
    	t.text :description
    	t.boolean :is_opened
    	t.integer :draw_count

      t.timestamps null: false
    end
  end
end
