class CreatePuzzlepieces < ActiveRecord::Migration
  def change
    create_table :puzzlepieces do |t|
    	t.integer :user_id
    	t.integer :lottery_id
    	t.integer :puzzle_id
    	t.integer :position_number
      t.timestamps null: false
    end

    add_index :puzzlepieces, :user_id
    add_index :puzzlepieces, :lottery_id
    add_index :puzzlepieces, :puzzle_id    
  end
end
