class CreateLotteries < ActiveRecord::Migration
  def change
    create_table :lotteries do |t|
    	t.integer :user_id
    	t.string  :serial_number
      t.timestamps null: false
    end

    add_index :lotteries, :user_id
  end
end
