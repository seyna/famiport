# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150704030010) do

  create_table "activities", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.datetime "start_time"
    t.datetime "end_time"
    t.text     "description",        limit: 65535
    t.boolean  "is_opened",          limit: 1
    t.integer  "draw_count",         limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "cover_file_name",    limit: 255
    t.string   "cover_content_type", limit: 255
    t.integer  "cover_file_size",    limit: 4
    t.datetime "cover_updated_at"
  end

  create_table "imageassets", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.string   "asset_file_name",    limit: 255
    t.string   "asset_content_type", limit: 255
    t.integer  "asset_file_size",    limit: 4
    t.datetime "asset_updated_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "lotteries", force: :cascade do |t|
    t.integer  "user_id",       limit: 4
    t.string   "serial_number", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "lotteries", ["user_id"], name: "index_lotteries_on_user_id", using: :btree

  create_table "lotterywinners", force: :cascade do |t|
    t.integer  "activity_id", limit: 4
    t.integer  "user_id",     limit: 4
    t.integer  "prize_id",    limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "lotterywinners", ["activity_id"], name: "index_lotterywinners_on_activity_id", using: :btree
  add_index "lotterywinners", ["prize_id"], name: "index_lotterywinners_on_prize_id", using: :btree
  add_index "lotterywinners", ["user_id"], name: "index_lotterywinners_on_user_id", using: :btree

  create_table "prizes", force: :cascade do |t|
    t.text     "name",               limit: 65535
    t.text     "description",        limit: 65535
    t.integer  "puzzle_id",          limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "prizes", ["puzzle_id"], name: "index_prizes_on_puzzle_id", using: :btree

  create_table "puzzleimages", force: :cascade do |t|
    t.integer  "puzzle_id",          limit: 4
    t.integer  "position_number",    limit: 4
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "puzzleimages", ["puzzle_id"], name: "index_puzzleimages_on_puzzle_id", using: :btree

  create_table "puzzlepieces", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "lottery_id",      limit: 4
    t.integer  "puzzle_id",       limit: 4
    t.integer  "position_number", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "puzzlepieces", ["lottery_id"], name: "index_puzzlepieces_on_lottery_id", using: :btree
  add_index "puzzlepieces", ["puzzle_id"], name: "index_puzzlepieces_on_puzzle_id", using: :btree
  add_index "puzzlepieces", ["user_id"], name: "index_puzzlepieces_on_user_id", using: :btree

  create_table "puzzles", force: :cascade do |t|
    t.integer  "activity_id",  limit: 4
    t.string   "name",         limit: 255
    t.integer  "total_pieces", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "puzzles", ["activity_id"], name: "index_puzzles_on_activity_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "serial_number",          limit: 255
    t.string   "phone_number",           limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
