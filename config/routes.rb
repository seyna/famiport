Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".  

  # You can have the root of your site routed with "root"
  root :to => "family#index"

  get "/event0708" => "family#index"
  get "/event0708/main"    => "family#main"
  get "/event0708/extra"   => "family#extra"
  get '/event0708/records' => "family#records"
  get '/event0708/notify'  => "family#notify"
  post '/event0708/lucky'  => "family#lucky"  

  get '/event0708/emails'  =>  "family#emails"
  get '/event0708/phones'  =>  "family#phones"
  post '/event0708/iscomfirmed'  =>  "family#iscomfirmed"

    #root :to => "family#device"  
    # get "/" => 'family#device'
    
    # get 'pc' => 'family#pc'
    
    # get 'mobile' => 'family#mobile'
    

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
