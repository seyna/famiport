# Preview all emails at http://localhost:3000/rails/mailers/fami_mailer
class FamiMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/fami_mailer/notify_winner
  def notify_winner
    FamiMailer.notify_winner
  end

end
