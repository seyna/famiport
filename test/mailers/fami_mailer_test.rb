require 'test_helper'

class FamiMailerTest < ActionMailer::TestCase
  test "notify_winner" do
    mail = FamiMailer.notify_winner
    assert_equal "Notify winner", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
