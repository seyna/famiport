class FamiMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.fami_mailer.notify_winner.subject
  #
  def notify_winner(user, prize)
  	month = Time.now.in_time_zone('Taipei').month
  	day = Time.now.in_time_zone('Taipei').day
    @from_date = month.to_s + " 月 " + day.to_s + " 日"
    @prize = prize.name

    mail(:to => user.email, :subject => "中獎通知")
  end
end
