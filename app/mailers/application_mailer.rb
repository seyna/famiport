class ApplicationMailer < ActionMailer::Base
  default from: '"全家系統管理員" <familynet-service@aremember.com>'
  layout 'mailer'
end
