class FamilyController < ApplicationController
	layout "famiport"
	before_filter :set_user, :only => [:main, :records, :lucky]
	before_filter :set_prize_bg_images, :only => [:main, :records, :lucky]	

	def index    
		@index = "active"
	end	

	def emails
		@emails = User.all.map(&:email)	
		render :text => @emails.to_json
	end

	def phones
		@phones = User.all.map(&:phone)	
		render :text => @phones.to_json
	end

	def iscomfirmed
		if User.find_by_phone_number(params[:phonenumber].to_s).confirmed_at == nil
			render :text => false
		else
			render :text => true
		end		
	end


	def records
		unless current_user
			redirect_to new_user_session_url
		else
			@lottery = Lottery.new
			@records = "active"
			@user_lotteries = @user.lotteries
			@user_pieces = @user.puzzlepieces
		end

		prize_ids = Lotterywinner.where(:user_id => @user.id).map(&:prize_id).uniq
		if prize_ids.size > 1
			@awards = Prize.where(:id=> prize_ids).map(&:name).join("、").to_s
		elsif prize_ids.size == 1
			@awards = Prize.where(:id=> prize_ids).last.name
		else
			@awards = nil
		end			
	end
	
	def notify
		if current_user			
			if Lotterywinner.where(:user_id => current_user.id).size > 0
				prize = Lotterywinner.where(:user_id => current_user.id).last.prize
				FamiMailer.notify_winner(User.find(current_user.id), prize).deliver_now!
			end
		else
			redirect_to event0708_records_url
		end
	end

	def extra	
		@extra = "active"
	end

	def main		
		@lottery = Lottery.new
		@main = "active"
	end	

	def lucky		
		month = Time.now.in_time_zone('Taipei').month
  	day = Time.now.in_time_zone('Taipei').day
		@from_date = month.to_s + " 月 " + day.to_s + " 日"
		if current_user
			serial_number = params[:lottery][:serial_number].upcase

			lottery_histroy = @user.lotteries.map(&:serial_number)

			# case 1: 
			#   if user did input lottery before
			#   redirect and show message

			if lottery_histroy.include?(serial_number)
				@lottery = Lottery.find_by_serial_number(serial_number)
				render "family/input_before"			
			elsif (/^[X][CDE]\d{8}$/.match(serial_number) && serial_number.length == 10)  || (/^YK\d{8}$/.match(serial_number) && serial_number.length == 10) || (/^FM\d{8}$/.match(serial_number) && serial_number.length == 10)
				# now we draw and give pieces
				@lottery = Lottery.create(:user_id => @user.id, :serial_number => serial_number)			

				@puzzle_id_array = [1,2,3,4]  # change [1,2,3,4] according to puzzle_id as needed
				@puzzles = @puzzle_id_array.sample(3)  
				@puzzles.each_with_index do |pid, index|
					position_number = [1,1,1,1,1,1,1,1,1,2,2,2,3,4,5,5,5,6,7,8,9].sample(1).first					
					position_number = [1,2,3,4,5,6,7,8,9].sample(1).first if pid == 1
					cur = @user.puzzlepieces.where(:puzzle_id => pid).map(&:position_number)

					# find out if there is prize remained to draw
					prize_remained_count = 65 - Lotterywinner.where(:prize_id => 1).size if pid == 1					
					prize_remained_count =  1 - Lotterywinner.where(:prize_id => 2).size if pid == 2
					prize_remained_count =  2 - Lotterywinner.where(:prize_id => 3).size if pid == 3
					prize_remained_count =  1 - Lotterywinner.where(:prize_id => 4).size if pid == 4					

					if @user.puzzlepieces.where(:puzzle_id => pid).map(&:position_number).uniq.size == 8
						if (cur<<position_number).uniq.size == 9 && prize_remained_count > 0  # important
							position_number = [1,1,1,1,1,1,1,1,1,2,2,2,3,4,5,5,5,6,7,8,9].sample(1).first
							position_number = [1,2,3,4,5,6,7,8,9].sample(1).first if pid == 1
						else
							position_number = cur.sample(1).first
						end
					end

					Puzzlepiece.create( :user_id => @user.id, :lottery_id => @lottery.id, :puzzle_id => pid, :position_number => position_number)
				end
				@user_get_pieces = Puzzlepiece.where(:id => @user.puzzlepieces.last(3).map(&:id))

				# to check user win lottery or not
				@lucky = 0
				@puzzle_id_array.each do |pid|
					if @user.puzzlepieces.where(:puzzle_id => pid).map(&:position_number).uniq.size == 9
						@lucky = 1
						@win_prize = Prize.find(pid)
						@before = Lotterywinner.where(:user_id => @user.id).map(&:prize_id).include?(pid)
					end				
				end
				

				# case 2:
				#   if the lottery input was not in history record  &&  'lucky'
				if @lucky == 1 && (@before == false )
					Lotterywinner.create(:activity_id => 1, :user_id => @user.id, :prize_id => @win_prize.id)
					FamiMailer.notify_winner(User.find(current_user.id), @win_prize).deliver_now!
					render "family/lucky"					
				# case 3:
				#   if the lottery input was not in history record  &&  not  'lucky'
				else
					render "family/unlucky"
				end					
			else
				@serial_number = serial_number
				render "family/other_input_error"
			end
		
		else
			redirect_to new_user_session_url
		end				
	end

	private
	def set_user
		if current_user
			@user = User.find(current_user.id)
		else
			@user = User.new
		end		
	end

	def set_prize_bg_images
		@prize_bg_images = [
			'http://famiport-project.s3.amazonaws.com/prizes/images/000/000/001/thumb/20150615033130XX.jpg?1435994519',
			'http://famiport-project.s3.amazonaws.com/prizes/images/000/000/002/thumb/20150615032838RB.jpg?1435994505',
			'http://famiport-project.s3.amazonaws.com/prizes/images/000/000/003/thumb/20150615033559JZ.jpg?1435994379',
			'http://famiport-project.s3.amazonaws.com/prizes/images/000/000/004/thumb/20150615033213JV.jpg?1435994408' 
		]
	end

end
