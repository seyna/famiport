class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
	layout :layout_by_resource

	def layout_by_resource
	  if devise_controller? && resource_name == :user && action_name == "new"
	    "famiport"
    elsif devise_controller? && resource_name == :user && action_name == "edit"
			"famiport"
	  else
	    "application"
	  end
	end

	def configure_permitted_parameters
    #devise_parameter_sanitizer.for(:sign_up) << :phone_number
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:phone_number, :email, :password, :password_confirmation) }
  end

  def after_sign_in_path_for(resource)
	  event0708_records_path
	end

	def after_sign_out_path_for(resource_or_scope)
    event0708_path
  end

end
