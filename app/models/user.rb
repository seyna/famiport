class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :lotterywinners
  has_many :puzzlepieces
  has_many :lotteries

  def phone
  	self.phone_number
  end

end
