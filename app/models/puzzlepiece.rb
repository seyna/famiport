class Puzzlepiece < ActiveRecord::Base
	belongs_to :user
	belongs_to :lottery
	belongs_to :puzzle
end
