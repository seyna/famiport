class Puzzle < ActiveRecord::Base
	has_many :prizes
	belongs_to :activity
end
